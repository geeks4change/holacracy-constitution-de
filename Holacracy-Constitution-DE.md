**Holakratie Verfassung**

## Präambel

Die ***Gründer*** beschließen diese ***Verfassung*** als formelle Regelungsstruktur der angegebenen ***Organisation***. Die Gründer und alle anderen ***Organisationsmitglieder*** verpflichten sich, sich an der beschriebenen Strukturgebung zu beteiligen, und in Übereinstimmung mit den festgelegten Regeln und Abläufen zu handeln.

Mit der gleichen Rechtsgrundlage wie bei der Einführung, können die Gründer oder ihre Nachfolger diese Verfassung ändern oder aufheben. Änderungen müssen schriftlich erfolgen und allen Organisationsmitgliedern bekannt gemacht werden.

## Artikel 1: Organisationsstruktur

### 1.1 Definition *Rolle*

Eine ***Rolle*** ist ein Organisationskonstrukt, das jemand bekleiden und im Namen der Organisation mit Leben füllen kann.  Eine ***Rollenbeschreibung*** besteht aus einem aussagekräftigen Namen und einem oder mehreren der folgenden Bestandteile:

- **(a)** eine ***Zielsetzung***, also ein zu verfolgendes Ziel, ein Potenzial, oder eine Möglichkeit.
- **(b)** eine oder mehrere ***Befugnisse*** und ***Hoheiten***, also die Verfügung (*Befugnis*) oder ausschließliche Verfügung (*Hoheit*) über Geld, Gegenstände, Prozesse oder andere Dinge.
- **(c)** eine oder mehrere ***Verantwortlichkeiten***, also laufende Aktivitäten, um die Zielsetzung zu verfolgen oder andere Rollen zu unterstützen.

Die Rollenbeschreibung kann ***Richtlinien*** enthalten, die Befugnisse oder Hoheiten ausweiten oder einschränken, oder die Tätigkeiten der Rolle Regeln unterwerfen können.

### 1.2 Rollenzuweisung

Eine Rollenbeschreibung kann die Befugnis zur Besetzung einer anderen Rolle enthalten. Jede Person mit einer solchen Befugnis kann die zu besetzende Rolle jeder Person zuweisen, die sie einnehmen möchte, auch mehreren Personen gleichzeitig. Jede Person, die eine Rolle annimmt, wird ihr ***Rollenträger***. Ein Rollenträger kann später jederzeit zurücktreten, es sei denn, es wurde etwas anderes vereinbart. Wer die Befugnis zur Besetzung einer Rolle hat, kann auch jederzeit ein Rollenträger aus der Rolle entlassen. Eine Richtlinie kann Rollenzuweisungen oder -entlassungen weiter einschränken.

##### 1.2.1 Standardzuweisung

Wenn eine Rolle nicht besetzt ist, wird jede Person, die die Befugnis zur Besetzung dieser Rolle hat, automatisch auch als Rollenträger der nicht besetzten Rolle betrachtet.

Wenn unter den Rollenträgern keine Organisationsmitglieder sind, dann wird jedes Organisationsmitglied, das die Aufgabe zuweist, automatisch auch als Träger der Rolle betrachtet, falls und insofern die zugewiesenen Personen ihre Verantwortlichkeiten und Pflichten nicht aktiv ausfüllen.

##### 1.2.2 Rolle mit Schwerpunktsetzung

Die Zuweisung einer Rolle kann von der zuweisenden Person mit einem Schwerpunkt auf einen bestimmten Bereich oder Kontext versehen werden, wenn alle Elemente der Rollenbeschreibung innerhalb dieses Schwerpunktes Sinn ergeben. Wenn eine Rolle mit einem Schwerpunkt versehen wird, wird sie wie eine eigenständige Rolle behandelt, und Zielsetzung, Verantwortlichkeiten, Befugnisse und Hoheiten der Rolle gelten nur innerhalb des Fokus, der für diese Rollenzuweisung festgelegt wurde.


### 1.3 Verpflichtungen als Rollenträger

Wenn du eine Rolle einnimmst, hast du folgende Verpflichtungen:

##### 1.3.1 Abarbeiten von Spannungen

Du bist dafür verantwortlich, den aktuellen Stand von Zielsetzung und Verantwortlichkeiten deiner Rolle mit deiner Vision ihres vollen Potenzials zu vergleichen, und Abweichungen zwischen den beiden zu erfassen (jede Abweichung ist eine ***Spannung***). Du bist dann dafür verantwortlich, zu versuchen, diese Spannungen aufzulösen.

##### 1.3.2 Abarbeiten von Zielsetzung und Verantwortlichkeiten

Du bist dafür verantwortlich, regelmäßig Zielsetzung und jede einzelne Verantwortlichkeit deiner Rolle zu prüfen, und folgende Punkte zu formulieren:

- **(a)** ***Nächste Schritte***, also Handlungen, die du (bis auf Priorisierung) sofort umsetzen kannst; und
- **(b)** ***Projekte***, also konkrete Ergebnisse, auf die du (bis auf Priorisierung) direkt hinarbeiten kannst.


##### 1.3.3 Projekte herunterbrechen.

Als Rollenträger bist du dafür verantwortlich, regelmäßig die nächsten Schritte für jedes aktive Projekt deiner Rolle zu definieren.

##### 1.3.4 Projekte überwachen, nächste Schritte & Spannungen

Als Rollenträger bist du dafür verantwortlich, alle Projekte und nächste Schritte für deine Rolle in schriftlichen Listen aufzuzeichnen und zu überwachen. Du musst auch Spannungen, die du auflösen willst, aufzeichnen, zumindest bis du sie zu Projekten oder nächsten Schritte verarbeitest. Du bist auch dafür verantwortlich, diese Listen regelmäßig zu überprüfen und zu aktualisieren, um sie als solide Quelle für die zukünftige Arbeit deiner Rolle zur Verfügung zu haben.

##### 1.3.5 Nächste Schritte ausführen

Wann immer du Zeit hast, in einer Rolle zu handeln, bist du dafür verantwortlich, die möglichen nächsten Schritte zu vergleichen und den mit dem größten Mehrwert für die Organisation auszuführen.


### 1.4 Kreise

Ein ***Kreis*** ist eine Einheit, in der Rollen und Richtlinien um eine Zielsetzung herum organisiert werden. Die Rollen und Richtlinien innerhalb eines Kreises bilden seine ***Arbeitsstruktur***.

#### 1.4.1 Herunterbrechen der Rollen

Jede Rolle kann intern einen Kreis bilden. Der innere Kreis einer Rolle kann Rollen und Richtlinien enthalten, um die Arbeit dieser Rolle herunterzubrechen und zu organisieren. Dies gilt nicht für die in Anhang A definierten Rollen, die nicht weiter heruntergebrochen werden dürfen.

Der innere Kreis einer Rolle wird als ein ***Unterkreis*** des breiteren Kreises angesehen, der die Rolle beinhaltet, während der umfassende Kreis der ***Oberkreis*** ist.

##### 1.4.2 Delegieren von Befugnissen und Hoheiten

Wenn ein Kreis eine Befugnis oder Hoheit an eine seiner Rollen vergibt, kann jedes Rollenträger diese Befugnis oder Hoheit im Namen des Kreises ausüben. Ein Kreis kann nur Befugnissen und Hoheiten an seine Rollen vergeben, die in die eigenen Befugnissen und Hoheiten des Kreises fallen, oder die nur innerhalb seiner eigenen internen Abläufe bedeutsam sind.

Sobald eine Rolle eine Befugnis oder Hoheit ausübt, kann sie Richtlinien erstellen, die diese Befugnis oder Hoheit innerhalb ihrer Arbeitsstruktur festlegen. Der Kreis, der die Befugnis oder Hoheit delegiert hat, behält sich jedoch das Recht vor, auch seine eigenen Richtlinien für diese Befugnis oder Hoheit festzulegen. Solche Richtlinien haben im Konfliktfall Vorrang vor den von der Rolle definierten Richtlinien.

Die Zuteilung einer Befugnis oder Hoheit an eine Rolle beinhaltet das Recht, Geld oder andere Werte auszugeben nur dann, wenn dies ausdrücklich festgelegt ist.

##### 1.4.3 Ankerkreis

Der breiteste Kreis, der die Zielsetzung der gesamten Organisation verfolgt, ist der ***Ankerkreis***. Der Ankerkreis übt alle Befugnisse und Hoheiten aus, über die die Organisation selbst verfügt, und hat keinen Oberkreis und keine Verantwortlichkeiten. Der Ankerkreis kann seine Zielsetzung durch eine Richtlinie ändern.

Die Gründer können eine anfängliche Struktur und andere Arbeitsstrukturen innerhalb des Ankerkreises festlegen, wenn sie diese Satzung verabschieden.

##### 1.4.4 Entsenden in den Ankerkreis

Eine Rolle kann in einen anderen Kreis entsendet werden, wenn eine Richtlinie dieses anderen Kreises oder eines seiner Oberkreise sie dazu einlädt.

Sobald eine Rolle in einen anderen Kreis entsandt wird, gilt sie als Teil der Arbeitsstruktur dieses anderen Kreises. Dieser Kreis kann die Rolle ergänzen und das so Ergänzte später ändern. Er darf jedoch weder die Rolle streichen noch etwas ändern, was von einem anderen Kreis angelegt wurde, noch darf ein anderer Kreis etwas ändern oder streichen, was dieser Kreis angelegt hat. Die Befugnis für Rollenzuweisungen und -änderungen bleibt beim Quell-Kreis. Der Kreis, in den eine Rolle entsandt wird, gilt nicht als ihr Oberkreis, und auch der innere Kreis der Rolle gilt nicht als ihr Unterkreis.

Ein Kreis kann die Entsendung einer Rolle aufheben, indem er die einladende Richtlinie aufhebt, oder durch einen anderen in dieser Richtlinie festgelegten Mechanismus. Eine Rolle kann auch selbst einen Kreis verlassen, in den sie entsandt wurde, es sei denn, eine vom Oberkreis erlassene oder ihn betreffende Richtlinie sieht etwas anderes vor. Sobald eine Rolle einen Kreis verlässt, erlöschen alle Teile der Rollenbeschreibung, die von diesem Kreis angelegt wurden.

##### 1.4.5 Kooperations-Kreise

Zwei oder mehr Rollen können jederzeit einen neuen Kreis gründen, um eine gemeinsame Zielsetzung der Rollen zu verfolgen. Dazu müssen sich die Gründerrollen auf eine Zielsetzung für den neuen Kreis einigen, die sie später durch eine Richtlinie ändern können. Der neue Kreis basiert also auf einer Richtlinie, die die Gründerrollen einlädt, und den Gründerrollen als Entsandte. Ein auf diese Weise geschaffener Kreis hat keinen Oberkreis, keine Verantwortlichkeiten und keine Befugnissen und Hoheiten, es sei denn, eine Richtlinie eines anderen Kreises überträgt ihm diese. Ein solcher Kreis wird automatisch aufgelöst, wenn er keine entsandten Rollen mehr hat.

### 1.5 Kreisleiter

Jedes Rollenträger nimmt auch automatisch die ***Kreisleiter-Rolle*** innerhalb des inneren Kreises dieser Rolle ein, und ist somit in dieser Funktion ***Kreisleiter***. Die Kreisleiter-Rolle hat die in Anhang A angegebene Rollenbeschreibung. Ein Kreis ohne Oberkreis hat keine Kreisleiter, es sei denn, eine Richtlinie des Kreises sagt etwas anderes.

##### 1.5.1 Prioritäten und Strategien festlegen

Ein Kreisleiter kann Handlungsfelder des Kreises gewichten und so rollenübergreifende Prioritätskonflikte entscheiden. Ein Kreisleiter kann auch eine oder mehrere ***Strategien*** als Deutungshilfe für die Prioritätensetzung im Kreis festlegen.

##### 1.5.2 Externe Bezüge umleiten

Wann immer eine Arbeitsstruktur außerhalb des Kreises Bezug auf den Kreis selbst oder eine Rolle im Kreis nimmt, kann ein Kreisleiter diesen Bezug umleiten, um stattdessen auf eine andere Rolle im Kreis zu verweisen. Diese Klarstellung gilt nicht als Änderung der Arbeitsstruktur des anderen Kreises.

#### 1.5.3 Ändern der Kreisleiterrolle

Ein Kreis darf weder die Zielsetzung seiner Kreisleiter-Rolle ändern, noch die Rolle streichen.

Ein Kreis kann Verantwortlichkeiten oder Befugnisse und Hoheiten zu seiner Kreisleiter Rollenbeschreibung hinzufügen, und diese Zusätze gelten automatisch auch (rekursiv) für die Kreisleiter-Rolle jedes Unterkreises. Ein Kreis darf keine Verantwortlichkeiten oder Befugnisse und Hoheiten nur zu seiner eigenen Kreisleiter-Rolle hinzufügen.

Ein Kreis kann Verantwortlichkeiten, Befugnisse, Hoheiten oder Funktionen aus seiner eigenen Kreisleiter-Rollenbeschreibung streichen: Entweder indem er diese Elemente auf eine andere Rolle des Kreises überträgt, oder indem er sie auf eine andere Art und Weise sicherstellt. Auf diese Weise wird das entsprechende Element automatisch aus der Rolle des Kreisleiters des Kreises gestrichen, so lange diese Delegation in Kraft ist.

## Artikel 2: Verpflichtungen gegenüber anderen

Als Rollenträger hast du die folgenden Verpflichtungen gegenüber anderen Rollenträgern der Organisation, in Ausübung ihrer Funktion.

### 2.1 Verpflichtung zu Transparenz

Du hast die Pflicht, auf Anfrage des Rollenträgers Einblick in beliebige folgende Punkte zu geben:

- **(a) Projekte und nächste Schritte:** Du musst alle Projekte und nächsten Schritte, die du für deine Rollen verfolgst, benennen.
- **(b) Relative Priorität:** Du musst für die Projekte und nächsten Schritten  deiner Rolle, sowie alle anderen um deine Aufmerksamkeit konkurrierenden Dinge, eine Prioritäts-Rangfolge benennen.
- **(c) Prognosen:** Du musst eine Prognose abgeben, wann eines der Projekte oder nächsten Schritte deiner Rolle voraussichtlich abgeschlossen ist. Eine grobe Schätzung, unter Einbeziehung deiner aktuellen Situation und Prioritäten. reicht aus. Eine detaillierte Analyse oder Planung ist nicht erforderlich, und diese Prognose stellt in keiner Weise eine Verpflichtung dar. Sofern die Arbeitsstruktur nichts anderes vorschreibt, bist du nicht zum Nachhalten der Prognose oder zum Rückmeldung von Änderungen an den Empfänger verpflichtet.
- **(d) Checklisten-Aktionen:** Du musst den Abschluss aller wiederkehrenden Aktionen bestätigen, die du für deine Rollen oder als Organisationsmitglied durchführst. Auf Anfrage musst du diese Bestätigungen regelmäßig wiederholen, bis du annehmen kannst, dass sie nicht mehr von Nutzen sind.
- **(e) Kennzahlen:** Du musst alle Kennzahlen, die du in deinen Rollen oder als Organisationsmitglied sammelst, zugänglich machen. Auf Anfrage musst du diese Kennzahlen so lange regelmäßig weitergeben, bis du erwarten kannst, dass sie nicht mehr nützlich sind.
- **(f) Fortschritts-Updates:** Du musst eine Zusammenfassung der Fortschritte mitteilen, die du in deiner Rolle oder in einem Projekt deiner Rolle seit der letzten Aktualisierung, gemacht hast. Falls gewünscht, musst du diese Aktualisierungen weiterhin regelmäßig teilen, bis du annehmen kannst, dass sie nicht mehr nützlich sind.

### 2.2 Pflicht zum Bearbeiten

Du hast die Pflicht, Nachrichten und Anfragen anderer Rollenträger umgehend wie folgt abzuarbeiten:

- **(a) Anfragen zur Bearbeitung:** Andere Rollenträger könnten dich bitten, den Zielsetzung oder einer Verantwortlichkeit oder einem Projekt deiner Rolle nachzugehen. Du musst dann einen geeigneten nächsten Schritt festlegen und mitteilen, falls es einen solchen gibt. Wenn es keinen gibt, musst du stattdessen mitteilen, worauf du wartest, bevor du einen nächsten Schritt durchführen kannst. Wenn der nächste Schritt oder das Warten-auf, das du benennst, Teil eines umfassenderen Vorhabens deiner Rolle ist, musst du auch das als Projekt erfassen und mitteilen.
- **(b) Anfragen für Projekte & Nächste Schritte:** Andere könnten dich anfragen, eine bestimmte nächste Aktion oder ein Projekt in deiner Rolle zu übernehmen. Du musst diese annehmen und weiterverfolgen, wenn du annehmen kannst, dass es (grundsätzlich, also bis auf Prioritätensetzung)) sinnvoll wäre, in deiner Rolle darauf hinzuarbeiten. Wenn du das nicht tust, dann musst du entweder deine Gründe erklären oder etwas anderes vorschlagen, von dem du glaubst, dass es stattdessen das Ziel des Antragstellers erreichen wird.
- **(c) Anfragen auf Gewährung einer Befugnis oder Hoheit:** Andere Rollenträger können dich anfragen, ihnen Teile der Befugnis oder Hoheit deiner Rolle zu gewähren. Du musst dies gewähren, wenn du darin keine Einschränkung deiner Möglichkeit siehst, die Zielsetzung oder Verantwortlichkeiten deiner Rolle zu erfüllen. Wenn du eine solche Einschränkung siehst, musst du sie der anfragenden Person mitteilen.
- **(d) Informationsanfragen:** Andere Rollenträger können dir Fragen stellen oder Informationen anfordern, die für deine Rolle bedeutsam sind. Du musst das nach bestem Wissen und Gewissen, zumindest mit kurzen Antworten oder leicht zugänglichen Informationen, beantworten.

### 2.3 Pflicht zur Prioritätensetzung

Du hast die Pflicht, deine Aufmerksamkeit in folgender Weise zu priorisieren:

- **(a) Anfragen bearbeiten:** Du musst generell der Bearbeitung eingehender Nachrichten anderer Rollen Vorrang vor der Ausführung deiner eigenen nächsten Schritte einräumen. Du kannst jedoch die Verarbeitung von Nachrichten so lange aufschieben, bis du sie zu einem geeigneten Zeitpunkt gesammelt verarbeiten kannst, solange die Bearbeitung noch zeitnah erfolgt. Bearbeitung beinhaltet, dass du alle in diesem Artikel genannten Pflichten erfüllst, und dann auf Anfrage mitteilst, wie du die Nachricht verarbeitet hast. Die Verarbeitung beinhaltet nicht das Ausführen der nächsten Schritte oder Projekte, die du erfasst hast.
- **(b) Meetings:** Du musst die Teilnahme an einem Meeting, das in dieser Verfassung definiert ist, gegenüber der Durchführung deiner eigenen nächsten Schritte priorisieren, aber nur, wenn diese Priorisierung ausdrücklich für ein bestimmtes Meeting verlangt wird. Du kannst die Anfrage ablehnen, wenn du bereits eine andere Vereinbarung für die Zeit des Meetings hast.
- **(c) Prioritäten des Kreises:** Bei der Auswahl dessen, woran du arbeitest, musst du alle offiziellen Strategien oder relativen Priorisierungen des Kreises, zu dem deine Rolle gehört, oder eines Oberkreises davon, berücksichtigen. Offizielle Prioritäten sind diejenigen, die von einem Kreisleiter oder anderen mit Prioritätenklärung oder Strategiesetzung befugten Rollen oder Abläufen festgelegt werden. Du musst diese offiziellen Prioritäten dann als wichtiger für die Organisation behandeln als deine eigenen individuellen Prioritäten oder dein eigenes Gefühl für die Prioritäten der Organisation.
- **(d) Fristen:** Wenn die Arbeitsstruktur oder eine offizielle Strategie oder Prioritätensetzung eines Kreises eine Frist enthält, die angibt, bis wann etwas getan werden muss, musst du diese Frist nicht unbedingt einhalten. Stattdessen musst du dies als eine offizielle Priorisierung der zur Frist-Einhaltung notwendigen Tätigkeiten gegenüber allen anderen Tätigkeiten für diesen Kreis interpretieren und entsprechend handeln. Ein Leiter des Kreises oder eine andere Rolle oder ein anderer Ablauf mit der Befugnis, Prioritätskonflikte zwischen den Rollen zu lösen, kann diese Prioritätensetzung außer Kraft setzen.

## Artikel 3: Operative Meetings

Im Ausübung seiner Rolle kann jedes Organisationsmitglied ein ***Operatives Meeting*** einberufen, um andere Rollen in ihren Verantwortung und Pflichten anzufordern.

### 3.1 Teilnahme

Das Organisationsmitglied, das ein operatives Meeting einberuft, muss die gewünschten Rollen in diesem Meeting benennen. Alle Organisationsmitglieder, die als Rollenträger dieser Rollen fungieren, werden dann zur Teilnahme eingeladen, es sei denn, das Einberufende beschränkt die Einladung so, dass sie nur eine Teilmenge von Rollenträgern für eine Rolle enthält.

### 3.2 Ablauf des Meetings

Das Organisationsmitglied, das ein operatives Meeting einberuft, muss eine Person finden, die es moderiert. Sofern in keiner Richtlinie etwas anderes festgelegt ist, muss diese Person dazu folgenden Ablauf anwenden:

- **(a) Ankommens-Runde:** Jedes Teilnehmer teilt der Reihe nach seinen aktuellen Stand mit oder gibt einen anderen Eröffnungskommentar für das Meeting ab. Kommentare sind nicht erlaubt.
- **(b) Checklisten-Berichte:** Jedes Teilnehmer bestätigt den Abschluss aller wiederkehrenden Aktionen, über die es regelmäßig für seine Rolle im Meeting berichtet.
- **(c) Kennzahlen-Berichte:** Jedes Teilnehmer teilt alle Kennzahlen mit, über die es regelmäßig für seine Rolle im Meeting berichtet.
- **(d) Fortschrittsmeldungen:** Jedes Teilnehmer nennt die Fortschritte in einem Projekt oder einer anderen Initiative, über die es regelmäßig für seine Rolle im Meeting berichtet. Das Teilnehmer teilen nur den Fortschritt, der seit einem früheren Bericht gemacht wurde, und nicht den allgemeinen Status der Arbeit.
- **(e) Tagesordnung erstellen:** Die Teilnehmer erstellen eine Tagesordnung mit Punkten, die im Meeting abgearbeitet werden. Jedes Teilnehmer kann so viele Punkte auf die Tagesordnung setzen, wie es möchte, indem es jedem Punkt eine kurze Bezeichnung gibt. Erklärungen und Diskussionen sind hier nicht erlaubt. Weitere Tagesordnungspunkte können nach zwischen den anderen Tagesordnungspunkten eingebracht werden.
- **(f) Vorsortieren von Spannungen:** Um seinen Tagesordnungspunkt zu bearbeiten, kann das Initiator des Tagesordnungspunktes Anfragen an ein anderes Teilnehmer richten, entweder in seiner allgemeinen Eigenschaft als Organisationsmitglied oder an eine Rolle, die dieses Teilnehmer im Meeting vertritt. Anfragen an eine Rolle können jedoch nur im Rahmen einer Rolle gestellt werden, die das Antragsteller im Meeting vertritt. Die Person, die das Meeting moderiert, kümmert sich um die Zeit, die für jeden Tagesordnungspunkt zur Verfügung steht, um Platz für die gesamte Tagesordnung zu schaffen, und kann die Bearbeitung jedes Punktes nach dem entsprechenden Anteil der Meetingzeit unterbrechen.
- **(g) Abschlussrunde:** Jedes Teilnehmer gibt der Reihe nach eine abschließende Reflektion über das Meeting. Kommentare sind nicht erlaubt.

Eine Richtlinie eines Kreises kann für von einer bestimmten Rolle des Kreises einberufenes operatives Meeting einen alternativen Ablauf festlegen oder diesen Standardablauf ändern.

### 3.3 Stellvertretung abwesender Teilnehmer

Wenn eine zu einem operativen Meeting eingeladene Rolle des Kreises aus irgendeinem Grund nicht im Meeting vertreten ist, kann stattdessen ein Kreisleiter dieses Kreises diese Rolle während des Meetings vertreten.

## Artikel 4: Übertragung aller Befugnisse

Mit der Annahme dieser Verfassung übertragen die Gründer ihre Befugnis, die Organisation zu führen und lenken, auf die Regeln und Abläufe dieser Verfassung. Davon ausgenommen sind alle Befugnisse, die die Gründer nicht delegieren können. Als Organisationsmitglied kannst du dich auf die von dieser Verfassung gewährten Befugnisse in dem vollen Ausmaß verlassen, in dem die Gründer diese Befugnisse hatten, bevor sie die Verfassung verabschiedeten.

### 4.1 Befugnisse der Rollenträger

Als Rollenträger hast du die Befugnis, alle Maßnahmen zu ergreifen oder Entscheidungen zu treffen, um die Zielsetzung oder Verantwortlichkeiten deiner Rolle zu erfüllen, solange du nicht gegen eine Regel dieser Verfassung verstößt.

Wenn du zwischen deinen Aktivitäten priorisierst, hast du die Befugnis, dein eigenes gesundes Urteilsvermögen über ihren relativen Wert für die Organisation zu fällen.

Du darfst dein gesundes Urteilsvermögen auch nutzen, um diese Verfassung und alles, was unter ihrer Befugnis steht, zu interpretieren. Du kannst ferner interpretieren, wie diese in deiner jeweiligen Situation, anzuwenden ist, und auf Grundlage deiner Interpretation handeln. Du musst jedoch die Regelungen der Arbeitsstruktur in den Zusammenhang von Zielsetzung und Verantwortlichkeiten ihres Kreises setzen. Du darfst keine Interpretationen verwenden, die im Widerspruch zu dieser eEinordnung steht.

#### 4.1.1 Verstoße nicht gegen Richtlinien

Während du in einer Rolle handelst, darfst du keine Richtlinien der Rolle selbst oder eines Kreises, der die Rolle enthält, verletzen.

##### 4.1.2 Hole Erlaubnis ein, bevor du in eine Befugnis oder Hoheit eingreifst

In Ausübung deiner Rolle darfst du deine Befugnisse und Hoheiten einsetzen.

Du darfst auf jede nicht weiter delegierte Befugnis oder Hoheit deines Kreises einwirken, oder in jede Befugnis oder Hoheit, auf die dein Kreis selbst einwirken darf. Wenn du allerdings annehmen musst, dass es sehr schwierig oder teuer sein würde, deinen Eingriff rückgängig zu machen, musst du zuvor Erlaubnis einholen.

Du darfst auf keine Befugnis oder Hoheit einwirken, die an eine Rolle oder einen Kreis delegiert wurde, die deine Rolle nicht enthält, es sei denn, du hast die Erlaubnis dazu. Du darfst dies auch nicht ohne Erlaubnis in Bezug auf eine Befugnis oder Hoheit tun, die eine andere souveräne Einheit ausübt.

Wenn du eine Erlaubnis benötigst, um in eine Befugnis oder Hoheit einzuwirken, kannst du sie von derjenigen Rolle erhalten, die diese Befugnis oder Hoheit ausübt. Du kannst auch die Erlaubnis erlangen, indem du deine Absicht ankündigst, eine bestimmte Aktion durchzuführen, und jede Rolle mit einer entsprechenden Befugnis oder Hoheit einlädst, Einwände zu erheben. Du musst dann eine angemessene Zeit auf eine Antwort warten. Wenn niemand in dieser Zeit Einwände erhebt, hast du die Erlaubnis, in alle Befugnisse oder Hoheiten einzuwirken, über die eine Rolle der Organisation verfügt, die deine Ankündigung erreicht hat. Du kannst davon ausgehen, dass eine schriftliche Ankündigung jede Person erreicht hat, die normalerweise Nachrichten in dem von dir benutzten Kanal liest. Jede so erteilte Erlaubnis gilt nur während der Durchführung der von dir angekündigten Aktion. Eine Richtlinie kann diesen Ablauf ändern oder einschränken.

##### 4.1.3 Hole Befugnis ein, bevor du Geld ausgibst

Während du eine Rolle ausübst, darfst du kein Geld oder andere Werte ausgeben, es sei denn, du bekommst vorher die Befugnis dazu. Diese Befugnis muss von einer Rolle oder einem Kreis kommen, der bereits die Befugnis hat, diese Vermögenswerte auszugeben. Es gilt wie eine Ausgabe, wenn du nennenswertes Eigentum des Kreises beeinträchtigst oder eines seiner Rechte erheblich einschränkst.

Um eine Erlaubnis für eine Ausgabe zu erhalten, musst du der Rolle oder dem Kreis, von dem du die Erlaubnis erhalten möchtest, schriftlich deine Ausgabe-Absicht bekannt geben. Du musst diese Ankündigung dort bekannt geben, wo alle Rollenträger bzw. Kreisleiter sie üblicherweise sehen. Deine Erklärung muss den Grund für die Ausgaben und die Rolle, in der du die Ausgaben tätigen willst, enthalten. Du musst dann eine angemessene Zeit für Überlegung und Antwort geben. Jedes Empfänger deiner Ankündigung kann die sie für zusätzliche Überlegungen eskalieren, und du darfst nicht mit der Ausgabe beginnen, wenn sie eskaliert ist. Allerdings kann ein Rollenträger der betroffenen Rolle oder Kreisleiter des betroffenen Kreises eine Eskalation rückgängig machen, genauso wie die Person, die sie eskaliert hat. Sobald eine angemessene Zeit verstrichen ist und keine Eskalation mehr besteht, darf deine Rolle über diese Mittel verfügen. Du kannst sie für deine erklärte Zielsetzung einsetzen oder andere dazu ermächtigen. Die Rolle oder der Kreis, von dem du die Erlaubnis erhalten hast, verliert ihrerseits diese Befugnis, jedoch kann ein Rollenträger der betroffenen Rolle oder Kreisleiter des betroffenen Kreises die Autorisierung jederzeit widerrufen.

Eine Richtlinie kann diesen Ablauf in irgendeiner Weise ändern oder eine Rolle direkt autorisieren, die Ausgaben der Mittel des Kreises zu bestimmen.

### 4.2 Selbsterlaubnis

Als Organisationsmitglied darfst du in bestimmten Fällen mit ***Selbsterlaubnis*** tätig werden, und über die Befugnisse deiner Rolle hinaus handeln oder gegen Regeln dieser Verfassung verstoßen.

##### 4.2.1 Erlaubte Situationen

Du darfst nur dann mit Selbsterlaubnis tätig werden, wenn alle folgenden Punkte zutreffen:

- **(a)** Du handelst in gutem Glauben, Zielsetzung oder Verantwortlichkeiten einer Rolle der Organisation zu dienen.
- **(b)** Du glaubst, dass deine Handlung mehr Spannung für die Organisation auflösen oder abwendet, als sie voraussichtlich erzeugt.
- **(c)** Deine Handlung verpflichtet die Organisation nicht zu Ausgaben, die über das hinausgehen, wozu du ohnehin befugt bist.
- **(d)** Wenn deine Handlung gegen Richtlinien verstößt, Hoheiten verletzt, oder Befugnisse überschreitet, hast du Grund zur Annahme, dass eine Verzögerung durch Einholen von Erlaubnis oder Änderung der Arbeitsstruktur nicht vertretbare Kosten verursachen würde.

##### 4.2.2 Kommunikation und Wiederherstellung

Wenn du mit Selbsterlaubnis tätig wirst, musst du deine Handlung den Rollenträgern erklären, die davon erheblich beeinträchtigt werden könnten. Auf Anfrage eines solchen Rollenträgers musst du weitere Maßnahmen ergreifen, um Spannungen, die durch deine Selbsterlaubnis entstanden sind, zu beheben. Auf Verlangen eines solchen Rollenträgers musst du außerdem davon absehen, dir eine vergleichbare Selbsterlaubnis zu geben.

Du musst der hier beschriebenen Kommunikation und Wiederherstellung Vorrang vor deiner regulären Arbeit einräumen. Ein Kreisleiter eines Kreises, der alle von deiner Aktion betroffenen Rollen beinhaltet, kann diese Priorisierung jedoch ändern.

### 4.3 Gültigkeit von Altregelungen

Alle bestehenden Richtlinien und Systeme, die in der Organisation vor der Verabschiedung dieser Verfassung in Kraft waren, bleiben nach der Verabschiedung in vollem Umfang in Kraft, auch wenn sie Befugnisse oder Auflagen beinhalten, die nicht im Rahmen der Abläufe dieser Verfassung geschaffen wurden. Diese alten Richtlinien und Systeme dürfen jedoch nicht erweitert oder abgeändert werden und verlieren alle Bedeutung und Autorität, sobald die Abläufe dieser Verfassung etwas schaffen, das sie ersetzt oder ihnen widerspricht.

Darüber hinaus ergeben sich alle deine Verantwortlichkeiten und Einschränkungen als Organisationsmitglied aus dieser Verfassung und den Ergebnissen ihrer Abläufe, sowie aus deinen rechtlichen Verpflichtungen gegenüber der Organisation und dem Handeln in ihrem Namen. Keine impliziten Erwartungen oder Auflagen haben Bedeutung oder Autorität. Auch keine Weisungen, die unter den alten Regeln der Organisation vor der Annahme dieser Verfassung erlassen wurden.

## Artikel 5: Strukturierungs-Prozess

Um die Arbeitsstruktur eines Kreises zu ändern, muss der hier definierte ***Strukturierungs-Prozess*** verwendet werden.

### 5.1 Teilnehmer des Strukturierungs-Prozesses

Jeder Kreis hat eine Gruppe von ***Kreis-Mitgliedern***, die seine Rollen im Strukturierungs-Prozesses vertreten.

Die Mitglieder eines Kreises sind die Organisationsmitglieder, die entweder als Leiter des Kreises oder als Träger einer Rolle des Kreises fungieren. Wenn eine Rolle mehrere Rollenträger hat, kann der Kreis eine Richtlinie erlassen, die die Anzahl der Rollenträger im Strukturierungs-Prozess begrenzt.

#### 5.1.1 Kreisvertretung

Jedes Mitglied eines Kreises kann jederzeit die Wahl eines ***Kreisvertreters*** verlangen, das diesen Kreis innerhalb seines Oberkreises vertritt. Das gewählte Kreisvertreter bekleidet dort die in Anhang A definierte ***Kreisvertretungs-Rolle***.

Der Kreis muss den hier definierten integrativen Wahlprozess verwenden, um ein Kreisvertreter auszuwählen, es sei denn, eine Richtlinie legt einen anderen Ablauf fest. Als Kreisvertreter können nur Kreismitglieder des jeweiligen Kreises gewählt werden, jedoch nicht das Kreisleiter. Nicht mehr als eine Person kann gleichzeitig als Kreisvertreter eines Kreises fungieren, es sei denn, die Richtlinien des jeweiligen Kreises erlauben das.

Das ausgewählte Kreisvertreter wird Kreismitglied jedes Kreises, der die äußere Rolle des Kreises beinhaltet, mit der Befugnis, diese Rolle wie ein Rollenträger zu vertreten. Ein solcher Kreis kann diese Kreisvertreter durch eine Richtlinie von der Kreismitgliedschaft ausschließen oder die Kreismitgliedschaft einschränken, aber nur, wenn ihre Rollen eine andere Möglichkeit haben, eine vergleichbare Vertretung innerhalb des Kreises zu erhalten.

Ein Kreis kann Verantwortlichkeiten oder Hoheiten zu seiner eigenen Kreisvertretungs Rollenbeschreibung hinzufügen, sowie diese Zusätze ändern oder entfernen. Kein Kreis darf die Zielsetzung oder irgendwelche Verantwortlichkeiten in der Kreisvertretungs-Rolle ändern oder streichen, noch die Rolle selbst streichen.

##### 5.1.2 Moderation und Schriftführung

Jeder Kreis mit mehr als einem Kreismitglied enthält eine ***Moderations-Rolle*** und eine ***Schriftführungs-Rolle***, wie in Anhang A definiert. Die jeweils handelnde Person wird zu ***Moderation*** bzw. ***Schriftführung*** des Kreises. Jedes Kreismitglied kann jederzeit Wahl oder Neuwahl einer dieser Rollen verlangen.

Der Kreis muss das hier beschriebene integrative Wahlverfahren durchführen, um Moderation und Schriftführung auszuwählen. Keine Rolle oder Richtlinie darf diese Rollen zuweisen oder eine Zuweisung auf andere Weise aufheben oder diesen erforderlichen Ablauf ändern. In der Regel können nur Kreismitglieder gewählt werden. Eine Richtlinie des Kreises oder eines Oberkreises kann dies ausweiten oder einschränken.

Ein Kreis kann Verantwortlichkeiten, Befugnisse oder Hoheiten zur Rollenbeschreibung seiner Moderation oder Schriftführung ergänzen, sowie diese Ergänzungen ändern oder streichen. Kein Kreis darf Zielsetzung, Befugnisse und Hoheiten, noch irgendwelche Verantwortlichkeiten der Rolle ändern oder streichen, noch die Rolle selbst streichen.

### 5.2 Zulässige Ergebnisse des Strukturierungs-Prozesses

Im Strukturierungs-Prozess eines Kreises können die Kreismitglieder:

- **(a)** Rollen des Kreises definieren, ändern oder streichen; und
- **(b)** Richtlinien des Kreises definieren, ändern oder streichen; und
- **(c)** Rollen oder Richtlinien des Kreises in einen Unterkreis oder einen beliebigen Unterkreis davon verlagern, falls diese Zielsetzung oder Verantwortlichkeiten dieses Unterkreises umsetzen; und
- **(d)** Rollen oder Richtlinien aus einem Unterkreis oder irgendeinem Unterkreis davon in den Kreis selbst verlagern, falls diese nicht mehr relevant für Zielsetzung oder Verantwortlichkeiten dieses Unterkreises sind; und
- **(e)** Wahlen für jede gewählte Rolle innerhalb des Kreises abhalten.

Keine anderen Entscheidungen sind gültige Ergebnisse des Kreis-Strukturierungs-Prozesses.

##### 5.2.1 Zulässige Inhalte von Richtlinien

Eine Richtlinie darf ausschließlich eine oder mehrere der folgenden Bestandteile enthalten:

- **(a)** Beschränkungen von Befugnissen oder Hoheiten einer oder mehrerer der im Kreis enthaltenen Rollen; oder
- **(b)** Gewährungen von Befugnissen oder Hoheiten, die der Kreis oder das Kreisleiter innehat, an im Kreis enthaltene Rollen; oder
- **(c)** allgemeine Gewährungen oder Einschränkungen von Befugnissen oder Hoheiten im Rahmen der Befugnisse und Hoheiten des Kreises
- **(d)** Regeln, die eine Vorgaberegel oder einen Ablauf dieser Verfassung ändern, falls diese Änderung ausdrücklich erlaubt ist.

Eine Richtlinie, die Befugnisse gewährt oder einschränkt, gilt auch in allen Unterkreisen (rekursiv), sofern nicht anders angegeben. Eine Richtlinie, die eine Vorgaberegel oder einen Ablauf in der Verfassung ändert, gilt nur innerhalb des Kreises, in dem die Richtlinie gilt, oder, wenn dies ausdrücklich erlaubt ist, auch in allen Unterkreisen (rekursiv). Im letzteren Fall kann ein Unterkreis diese Richtlinie durch eine eigene Richtlinie außer Kraft setzen, es sei denn, dies ist in der ursprünglichen Richtlinie ausdrücklich verboten.

##### 5.2.2 Ungültige Arbeitsstruktur aufheben

Jedes Kreismitglied kann die Schriftführung eines Kreises bitten, über die Gültigkeit eines Aspekts der Arbeitsstruktur innerhalb dieses Kreises oder eines seiner Unterkreise zu entscheiden. Falls die Schriftführung zu dem Schluss kommt, dass der Aspekt gegen diese Verfassung verstößt, muss sie diesen Aspekt aus den Unterlagen des Kreises streichen. Danach muss die Schriftführung unverzüglich allen Kreismitgliedern mitteilen, was und warum gestrichen wurde.

### 5.3 Ablauf des Strukturierungs-Prozesses

Jedes Mitglied eines Kreises kann den Strukturierungs-Prozess einleiten, indem es eine Änderung seiner Arbeitsstruktur vorschlägt. Um dies zu tun, muss das ***Vorschlagsträger*** zunächst einen ***Vorschlag*** schriftlich allen anderen Kreismitglieder mitteilen, über einen beliebigen von der Schriftführung zugelassenen Kommunikationskanal. Die anderen Kreismitglieder können dann klärende Fragen stellen, Rückmeldungen geben und Bedenken äußern. Ein Bedenken stellt einen ***Einwand*** dar, wenn es die unten genannten Kriterien erfüllt, und die Person, die es vorgebracht hat, ist das ***Einwandträger***.

Sobald jedes Kreismitglied bestätigt, dass es keine Einwände gegen den Vorschlag hat, ist er angenommen und die Arbeitsstruktur des Kreises entsprechend geändert. Wenn Einwände erhoben werden, müssen Vorschlagsträger und jedes Einwandträger einen Weg finden, die Einwände auszuräumen, bevor der Kreis den Vorschlag annehmen kann. Danach muss allen Mitgliedern des Kreises eine weitere Gelegenheit gegeben werden, Einwände zu erheben. Ein Kreis kann eine Richtlinie verabschieden, in der eine Frist für die Erhebung von Einwänden festgelegt wird, nach deren Ablauf davon ausgegangen wird, dass alle, die nicht geantwortet haben, keine Einwände haben.

Wenn ein Kreismitglied Vorschläge macht oder Einwände erhebt, darf es nur die Rollen im Kreis vertreten, die es entweder als Rollenträger oder als Kreisvertreter einnimmt. Ein Kreismitglied kann auch eine Rolle vertreten, deren Vertretung es vorübergehend von einem seiner Rollenträger übertragen bekommt, bis diese Erlaubnis ausläuft oder zurückgezogen wird.

##### 5.3.1 Kriterien für gültige Vorschläge

Damit ein Vorschlag gültig ist, muss sein Vorschlagsträger folgendes können:

- **(a)** eine Spannung (also eine Differenz zwischen aktuellem und gewünschtem Zustand) beschreiben, die der Vorschlag für eine der Rollen des Vorschlagsträgers aufgreift; und
- **(b)** ein Beispiel einer tatsächlichen vergangenen oder gegenwärtigen Situation mitteilen, das diese Spannung veranschaulicht; und
- **(c)** eine vernünftige Erklärung geben, wie der Vorschlag die Spannung in diesem Beispiel reduziert hätte.

Falls der Moderation zu irgendeinem Zeitpunkt deutlich wird, dass ein Vorschlag diese Kriterien nicht erfüllt, muss sie den Vorschlag verwerfen.

##### 5.3.2 Kriterien für gültige Einwände

Ein Bedenken gilt nur dann als Einwand, wenn das Einwandträger eine plausible Begründung dafür geben kann, warum der Vorschlag **alle** folgenden Kriterien erfüllt:

- **(a)** Der Vorschlag würde die Fähigkeit des Kreises verringern, seine Zielsetzung zu verfolgen oder Verantwortlichkeiten auszuüben.
- **(b)** Der Vorschlag würde die Fähigkeit des Einwandträgers einschränken, die Zielsetzung oder Verantwortlichkeit einer seiner Rollen im Kreis zu erfüllen, auch wenn das Einwandträger keine anderen Rollen in der Organisation einnähme.
- **(c)** Das Bedenken besteht nicht schon ohne den Vorschlag. Also würde durch die Annahme des Vorschlags eine neue Spannung entstehen.
- **(d)** Die negativen Konsequenzen würden zwangsläufig eintreten, oder so abrupt, dass der Kreis keine angemessene Gelegenheit hätte, zu reagieren, bevor erheblicher Schaden entstehen könnte.

Unabhängig von den oben genannten Kriterien gilt ein Bedenken immer als Einwand, wenn die Annahme des Vorschlags gegen eine Regel dieser Verfassung verstoßen würde.

##### 5.3.3 Einwände testen

Die Moderation kann prüfen, ob ein Bedenken als Einwand zählt, indem sie das Einwandträger fragt, ob und wie das Bedenken die erforderlichen Kriterien erfüllt. Bei der Bewertung der Antworten darf die Moderation nur beurteilen, ob das Einwandträger die Argumente für die einzelnen Kriterien logisch begründet hat. Die Moderation darf nicht über die Stichhaltigkeit der Begründung oder ihre Relevanz urteilen.

Wenn als Einwand geltend gemacht wird, dass die Annahme des Vorschlags gegen diese Verfassung verstoßen würde, kann die Moderation die Schriftführung des Kreises um eine Auslegung bitten, ob das zutrifft. Wenn die Schriftführung feststellt, dass dies nicht der Fall ist, muss die Moderation den Einwand zurückweisen.

##### 5.3.4 Regeln der Integration

Beim Versuch, einen Einwand zu klären, gelten die folgenden Regeln:

- **(a)** Die Moderation muss den Einwand prüfen, wenn dies von einem Kreismitglied verlangt wird. Erfüllt er die geforderten Kriterien nicht, muss die Moderation ihn ablehnen.
- **(b)** Das Einwandträger muss versuchen, eine Änderung des Vorschlags zu entwickeln, die den Einwand auflöst und trotzdem die Spannung des Vorschlagsträgers behebt. Wenn die Moderation glaubt, dass das Einwandträger dies nicht in redlicher Weise versucht, muss die Moderation den Einwand als aufgegeben betrachten und ihn verwerfen.
- **(c)** Jedes Mitglied des Kreises kann dem Antragsteller Fragen zu der Spannung hinter dem Vorschlag oder zu Beispielen stellen, die das Antragsteller zur Veranschaulichung der Spannung geteilt hat. Wenn die Moderation der Meinung ist, dass das Antragsteller nicht in redlicher Weise antwortet, muss die Moderation den Vorschlag als hinfällig betrachten.
- **(d)** Das Einwandträger kann einen geänderten Vorschlag machen und vernünftige Argumente dafür vorbringen, warum er die Spannung auflösen würde. Dann muss das Vorschlagträger auf Anfrage des Einwandträgers ein vernünftiges Argument dafür vorbringen, warum der geänderte Vorschlag die Spannung in mindestens einem der Beispiele, die das Vorschlagträger zur Veranschaulichung der Spannung benutzt hat, nicht auflösen würde. Falls erforderlich, kann das Antragsteller ein weiteres Beispiel hinzufügen, um zu veranschaulichen, warum der geänderte Vorschlag die Spannung nicht auflösen würde. Wenn die Moderation glaubt, dass das Antragsteller nicht in der Lage oder nicht willens ist, dies zu tun, muss die Moderation den Vorschlag als hinfällig betrachten.

##### 5.3.5 Integrativer Wahlprozess

Weiterhin kann jedes Kreismitglied den Strukturierungs-Prozess des Kreises einleiten, indem es die Wahl von Kreisvertretung, Moderation oder Schriftführung verlangt. Die amtierende Moderation muss dann den ***Integrativen Wahlprozess*** wie folgt durchführen:

- **(a) Rolle beschreiben:** Zuerst benennt die Moderation die fragliche Rolle und wählt einen Zeitraum für die Wahl aus. Die Moderation kann auch andere für die Wahl relevante Informationen präsentieren. Während dieses und des nächsten Schrittes darf niemand sich zu möglichen Kandidaten äußern.
- **(b) Kandidaten nominieren:** Jedes Kreismitglied benutzt einen Stimmzettel oder ein anderes privates Medium, um das wählbare Kandidat zu benennen, von dem das Kreismitglied glaubt, dass es am besten für die Rolle geeignet ist. Die Kreismitglieder müssen ihren Stimmzettel zusätzlich mit ihrem eigenen Namen versehen. Niemand darf sich der Stimme enthalten oder mehrere Personen benennen.
- **(c) Runde: Nominierungen vorstellen:** In diesem Schritt teilt die Moderation jede Nominierung allen Kreismitgliedern mit. Für jede Nominierung gibt die nominierende Person an, warum sie glaubt, dass die nominierte Person besonders gut für die Rolle geeignet ist. Kommentare sind nicht erlaubt. Die nominierende Person darf sich nicht zu anderen potentiellen Kandidaten äußern, abgesehen von der von ihr nominierten Person.
- **(d) Runde: Nominierung ändern:** Sobald alle Nominierungen bekannt sind, kann jedes Kreismitglied seine Nominierung ändern und den Grund für die Änderung erklären. Kommentare sind nicht erlaubt.
- **(e) Vorschlag machen:** Die Moderation zählt die Nominierungen und schlägt vor, das Kandidat mit den meisten Nominierungen zu wählen. Bei Stimmengleichheit kann die Moderation eine der folgenden Aktionen durchführen: (i) wenn nur eines der punktgleichen Kandidaten sich selbst nominiert hat, diese Person vorschlagen; oder (ii) wenn die Person, die gerade die Rolle einnimmt, unter den punktgleichen Kandidaten ist, diese Person vorschlagen; oder (iii) blind eines der punktgleichen Kandidaten nach dem Zufallsprinzip auswählen und diese Person vorschlagen; oder (iv) zum vorherigen Schritt zurückgehen und jedes Kreismitglied, das jemand anderen als eines der punktgleichen Kandidaten nominiert hat, bitten, diese Nominierung in eines der punktgleichen Kandidaten zu ändern.
- **(f) Einspruchsrunde:** Die Moderation fragt jedes Kreismitglied, ob es einen Einwand gegen den Vorschlag sieht. Wenn Einwände auftauchen, kann die Moderation entweder eine Diskussion zulassen, um zu versuchen, diese Einwände zu lösen, oder den Vorschlag verwerfen. Wenn der Vorschlag verworfen wird, muss die Moderation zum vorherigen Ablauf zurückgehen, alle Nominierungen für das verworfene Kandidat ignorieren und die Regeln des vorherigen Ablaufs anwenden, um stattdessen ein anderes Kandidat vorzuschlagen.

Ein Kreis kann eine Richtlinie verabschieden, um ein Zeitlimit für die Nominierung oder die Antwort auf einen Vorschlag während des integrativen Ablaufs festzulegen. Nach Ablauf dieser Frist muss die Moderation jedes Person, die nicht geantwortet hat, vom restlichen Ablauf des Prozesses ausschließen.

##### 5.3.6 Stellvertretung für Moderator und Schriftführung

Eine Vertretung kann als Moderation oder Schriftführung fungieren, solange die Rolle nicht besetzt ist. Eine Vertretung kann auch einspringen, wenn die reguläre Moderation oder Schriftführung bei Bedarf nicht verfügbar ist oder aus irgendeinem Grund eine Vertretung anfordert.

Wann immer eine Vertretung benötigt wird, ist die Vertretung in dieser Rangfolge:

- **(a)** jemand, der von der zu ersetzenden Person angegeben wurde; oder
- **(b)** für die Moderation, die amtierende Schriftführung des Kreises, und für die Schriftführung, die amtierende Moderation des Kreises; oder
- **(c)** das Kreisleiter des Kreises, oder, falls es mehrere davon gibt, das erste, das sich selbst zur Vertretung erklärt; oder
- **(d)** das erste Kreismitglied, das sich selbst zur Vertretung erklärt hat.

### 5.4 Strukturierungs-Meetings

Zusätzlich zur Bearbeitung von Vorschlägen, die asynchron außerhalb der Meetings gemacht werden, wird jeder Kreis auch regelmäßige ***Strukturierungs-Meetings*** abhalten, um den Strukturierungs-Prozess in Echtzeit umzusetzen. Zu jedem Zeitpunkt, an dem der Kreis einen Vorschlag asynchron bearbeitet, kann jedes Mitglied des Kreises verlangen, dass der Vorschlag stattdessen in einem Strukturierungs-Meeting in Echtzeit bearbeitet wird, und die asynchrone Bearbeitung muss dann sofort eingestellt werden.

Die Schriftführung des Kreises ist für die Terminierung der Strukturierungs-Meetings verantwortlich, und die Moderation ist dafür verantwortlich, die Strukturierungs-Meetings in Übereinstimmung mit den vorliegenden Regeln zu moderieren. Zusätzlich zu den regelmäßig stattfindenden Strukturierungs-Meetings des Kreises muss die Schriftführung auf Antrag eines beliebigen Kreismitglieds umgehend ein außerordentliches Strukturierungs-Meeting einberufen. Das Antragsteller kann außerdem den Zweck für dieses außerordentliche Strukturierungs-Meeting angeben und die Grenzen für Änderungen der Arbeitsstruktur festlegen. Dies kann die Fokussierung des Meetings auf eine bestimmte Spannung beinhalten oder die Beschränkung darauf, nur bestimmte Rollen zu ändern. In diesem Fall ist die Befugnis dieses speziellen Strukturierungs-Meetings darauf beschränkt, nur Vorschläge für den angegebenen Zweck zu bearbeiten und nur Änderungen innerhalb der angegebenen Grenzen vorzunehmen.

##### 5.4.1 Teilnahme

Alle Kreismitglieder eines Kreises können an den Strukturierungs-Meetings des Kreises teilnehmen. Die amtierende Moderation und Schriftführung können auch dann teilnehmen, wenn sie keine Kreismitglieder des Kreises sind. In diesem Fall werden sie für die Dauer des Meetings vorübergehende Kreismitglieder.

Als Kreisvertreter für einen Kreis kannst du jedes Organisationsmitglied zum Strukturierungs-Meeting des Oberkreises deines Kreises einladen. Du darfst diese Einladung jeweils nur an ein Organisationsmitglied auf einmal aussprechen und nur, um die Verarbeitung einer bestimmten Spannung des von dir vertretenen Kreises zu unterstützen. Du musst dabei diese Spannung auch selbst empfinden und Grund zur Annahme haben, dass es sinnvoll ist, sie im Kreis zu verarbeiten. Dein eingeladener Gast wird für die Dauer des Meetings oder bis du die Einladung zurückziehst, ein vorübergehendes Mitglied des Kreises. Dein Gast kann deinen Kreis zusammen mit dir im Meeting vertreten, aber nur während der Verarbeitung dieser spezifischen Spannung.

Niemand sonst darf an den Strukturierungs-Meetings eines Kreises teilnehmen.

#### 5.4.2 Einladung & Dauer

Ein Kreis kann ein Strukturierungs-Meeting nur dann durchführen, wenn die Schriftführung alle Mitglieder des Kreises rechtzeitig im Voraus über das Meeting informiert hat. Darüber hinaus benötigt ein Kreis kein Quorum, um ein Strukturierungs-Meeting durchzuführen, es sei denn, eine Richtlinie schreibt dies vor.

Strukturierungs-Meetings enden, sobald sie die ursprünglich von der Schriftführung festgelegte Dauer erreicht haben. Die Schriftführung kann diese Dauer innerhalb des Meetings verlängern, falls kein Kreismitglied widerspricht.

Jedes Kreismitglied, das ein Strukturierungs-Meeting ganz oder teilweise versäumt, zählt als Mitglied des Kreises, das die Möglichkeit hatte, Bedenken über die darin gemachten Vorschläge vorzubringen. Ein Kreis kann also Vorschläge in einem Strukturierungs-Meeting ohne Rücksicht auf abwesende Mitglieder annehmen.

##### 5.4.3 Ablauf

Die Moderation muss den folgenden Ablauf für die Strukturierungs-Meetings verwenden:

- **(a) Einstiegs-Runde:** Jedes Teilnehmer teilt der Reihe nach seinen aktuellen Stand mit, oder gibt einen anderen Eröffnungskommentar für das Meeting ab. Kommentare sind nicht erlaubt.
- **(b) Tagesordnung erstellen und verarbeiten:** Die Moderation erstellt eine Tagesordnung mit Spannungen, die es zu verarbeiten gilt, und verarbeitet dann jeden Tagesordnungspunkt der Reihe nach.
- **(c) Abschlussrunde:** Jedes Teilnehmer gibt der Reihe nach eine abschließende Reflektion über das Meeting. Kommentare sind nicht erlaubt.

Zu jedem Zeitpunkt während dieses Ablaufs kann jedes Teilnehmer eine ***Auszeit*** Pause beantragen. Die Moderation kann diesem Antrag stattgeben oder ihn ablehnen. Während der Auszeit können die Teilnehmer administrative Fragen oder die Regeln dieser Verfassung diskutieren. Sie dürfen die Auszeit nicht nutzen, um auf die Klärung einer Spannung, eines Vorschlags oder eines Einwandes hinzuarbeiten. Die Moderation kann die Auszeit jederzeit beenden und den normalen Ablauf des Meetings wieder aufnehmen.

Eine Richtlinie des Kreises kann zu diesem Ablauf beitragen, darf aber nicht im Widerspruch zu den in diesem Artikel der Verfassung definierten Regeln oder Anforderungen stehen.

##### 5.4.4 Tagesordnung

Die Moderation fragt alle Teilnehmer nach Tagesordnungspunkten, und erstellt eine Tagesordnung mit zu verarbeitenden Spannungen. Die Moderation muss dies im Meeting tun und nicht im Voraus. Jedes Teilnehmer kann so viele Punkte auf die Tagesordnung setzen, wie es möchte, indem es für jeden Punkt eine kurze Bezeichnung wählt, wobei keine Erläuterungen oder Diskussionen erlaubt sind. Die Teilnehmer können während des Meetings, zwischen der Behandlung der bestehenden Tagesordnungspunkte, weitere Tagesordnungspunkte einbringen.

Für ein reguläres Arbeitsstruktur-Meeting kann die Moderation die Reihenfolge wählen, in der die Tagesordnungspunkte behandelt werden. Auf Antrag eines Meeting-Teilnehmers muss jedoch jeder Tagesordnungspunkt, der zu einer Wahl führt, vor allen anderen behandelt werden. Für ein spezielles Arbeitsstruktur-Meeting, das auf Antrag eines Teilnehmers anberaumt wird, kann dieses Teilnehmer die Reihenfolge der Tagesordnung wählen.

Die Tagesordnungspunkte werden einer nach dem anderen abgearbeitet. Um einen Wahlantrag zu bearbeiten, benutzt die Moderation den Ablauf des Integrativen Wahlprozesses. Um alles andere zu bearbeiten, verwendet die Moderation den unten beschriebenen integrativen Entscheidungsprozess.

##### 5.4.5 Integrativer Entscheidungsprozess.

Die Moderation muss den ***Integrativen Entscheidungsfindungsprozess*** wie folgt durchführen:

- **(a) Vorschlag unterbreiten:** Zuerst beschreibt das Antragsteller die Spannung und macht einen Vorschlag, um diese zu lösen. Auf Wunsch des Antragstellers kann die Moderation anderen erlauben, bei der Ausarbeitung eines Vorschlags zu unterstützen. Allerdings muss die Moderation diese Unterstützung ausschließlich darauf konzentrieren, zu einem ersten Vorschlag zu kommen, der die Spannung des Antragstellers aufnimmt. Die Moderation muss verbieten, andere Spannungen oder Bedenken über den Vorschlag zu diskutieren.
- **(b) Klärende Fragen:** Sobald das Antragsteller einen Vorschlag macht, können andere Fragen stellen, um den Vorschlag oder die Spannung dahinter besser zu verstehen. Das Antragsteller kann jede Frage beantworten, muss es aber nicht. Die Moderation muss jede Rückmeldung oder Meinungsäußerung zu dem Vorschlag stoppen und jede Art von Diskussion unterbinden. Die Teilnehmer können, während dieses Schrittes oder zu jedem anderen Zeitpunkt, an dem das jeweilige Teilnehmer das Wort hat, die Schriftführung bitten, den Vorschlag vorzulesen oder bestehende Regelungen der Arbeitsstruktur vorzulegen, und die Schriftführung muss dem nachkommen.
- **(c) Rückmeldungs-Runde:** Als nächstes kann jedes Teilnehmer, außer dem Antragsteller, der Reihe nach Rückmeldungen zum Vorschlag geben. Die Moderation muss alle anderen Kommentare, Versuche, andere in einen Dialog zu verwickeln, und alle Rückmeldungen, die sich auf andere Rückmeldungen statt des Vorschlags selbst beziehen, sofort unterbinden.
- **(d) Klarstellung und Ergänzung:** Als nächstes kann das Antragsteller Kommentare zu den Rückmeldungen abgeben und Änderungen am Vorschlag vornehmen. Die primäre Absicht aller Änderungen muss jedoch sein, besser auf die Spannung des Antragstellers einzugehen, und nicht auf Spannungen, die von anderen aufgeworfen wurden. Die Moderation muss Kommentare von anderen Personen als dem Antragsteller oder der Schriftführung sofort unterbinden. Jegliche Aktivität der Schriftführung muss sich ausschließlich darauf konzentrieren, den geänderten Vorschlag zu erfassen.
- **(e) Einwand-Runde:** Als nächstes kann jedes Teilnehmer der Reihe nach Bedenken gegen die Annahme des Vorschlags vorbringen. Die Moderation muss jede Art von Diskussionen und Antworten unterbinden. Die Moderation kann nun die Einwände auf Gültigkeit testen und alle gültigen Einwände notieren. Wenn es keine gültigen Einwände gibt, ist der Vorschlag angenommen. 
- **(f) Integration:** Wenn es Einwände gibt, konzentriert sich die Moderation auf jeden Einwand, einen nach dem anderen. Für jeden einzelnen wird ein Brainstorming durchgeführt, um eine mögliche Änderung des Vorschlags zur Lösung des Einwandes zu finden. Die Moderation markiert einen Einwand, der gelöst wird, sobald das Einwandträger bestätigt, dass der geänderte Vorschlag den Einwand nicht mehr auslöst, und das Antragsteller bestätigt, dass dieser Vorschlag die Spannung noch behandelt. Während dieses Schrittes muss die Moderation die in diesem Artikel beschriebenen Integrationsregeln anwenden. Sobald alle Einwände gelöst sind, geht die Moderation mit dem geänderten Vorschlag zurück in die Einspruchsrunde.

### 5.5 Konflikte bei der Auslegung

Als Organisationsmitglied kann deine Interpretation dieser Verfassung und der Arbeitsstruktur der Organisation manchmal mit der eines anderen Mitglieds in Konflikt geraten. In diesem Fall kann jede Partei die Schriftführung eines betroffenen Kreises bitten, über die Auslegung zu entscheiden. Bevor eine Schriftführung antwortet, kann jede Partei ihre eigene Interpretation verwenden. Nachdem eine Schriftführung geantwortet hat, müssen sich alle der Entscheidung dieser Schriftführung anschließen, bis sich der entsprechende Text oder die Umstände ändern.

Nach der Entscheidung über eine Auslegung kann eine Schriftführung die Entscheidung und die Logik dahinter veröffentlichen. Wenn sie veröffentlicht wird, müssen die Schriftführung dieses Kreises und alle darin enthaltenen Kreise versuchen, sich bei allen zukünftigen Entscheidungen dieser Logik anzuschließen. Eine Schriftführung kann diese Entscheidung widerrufen, sobald ein zwingender neuer Umstand die Argumentation hinfällig macht.

Du kannst gegen die Entscheidung einer Schriftführung bei der Schriftführung jedes Oberkreises Berufung einlegen. Eine Schriftführung eines Oberkreises kann die Auslegung einer Unterkreis-Schriftführung außer Kraft setzen.

### 5.6 Prozessversagen

***Prozessversagen*** liegt vor, wenn ein Kreis ein Muster von Verhalten oder Leistungen zeigt, das gegen die Regeln dieser Verfassung verstößt. Die Moderation oder Schriftführung eines Kreises kann Prozessversagen in ihrem eigenen Kreis oder einem Unterkreis nach vernünftigem Ermessen feststellen.

##### 5.6.1 Versagen der Strukturierung

Die Moderation eines Kreises kann Prozessversagen im ihrem Kreis auch dann feststellen, wenn ein Vorschlag nicht zu einer Entscheidung führt, nachdem die Beteiligten eine angemessene Zeit und Mühe in die Suche nach einer Lösung investiert haben.

##### 5.6.2 Wiederherstellung des Prozesses

Sobald eine dazu befugte Instanz Prozessversagen eines Kreises feststellt, tritt folgendes in Kraft:

- **(a)** die Moderation erhält die Befugnis, die Gültigkeit und Bedeutsamkeit aller Begründungen zu beurteilen, die zur Bestätigung von Vorschlägen oder Einwänden im Kreis vorgebracht werden; und
- **(b)** die Moderation des Oberkreises wird mit einem Projekt zur Wiederherstellung des ordnungsgemäßen Ablaufs innerhalb des Kreises beauftragt; und
- **(c)** die Moderation des Oberkreises erhält die Befugnis, als Moderation oder Schriftführung des Kreises zu fungieren; und
- **(d)** die Moderation des Oberkreises kann dem Kreis ein zusätzliches Kreisleiter für die Dauer des Prozessversagens zuweisen. Jede Entscheidung, die diese Person als Kreisleiter trifft, ersetzt und unterbindet jede widersprüchliche Entscheidung eines anderen Kreisleiters.

Diese Befugnisse enden, sobald der ordnungsgemäße Prozess im Kreis wiederhergestellt ist, was vom Moderator des Oberkreises festgestellt wird.

Wenn der Kreis mit Prozessversagen keinen Oberkreis hat, dann gehen die oben genannten Befugnisse stattdessen auf die Moderation dieses Kreises über.

##### 5.6.3 Eskalation von Prozessversagen

Prozessversagen in einem Kreis wird nicht automatisch als Prozessversagen seines Oberkreises betrachtet. Bleibt das Prozessversagen jedoch über einen unangemessen langen Zeitraum ungelöst, dann wird auch der Oberkreis als von Prozessversagen betroffen betrachtet.

## Artikel 6: Organisationszugehörigkeit

### 6.1 Organisationsmitglieder

Die Organisation kann den Status eines Organisationsmitglieds jeder Person erteilen, die sich bereit erklärt, sich an alle maßgeblichen Bestimmungen dieser Verfassung zu halten. Dadurch entsteht eine ***Organisationsmitglied-Beziehung*** zwischen dieser Person und der Organisation. Alle Versprechungen, die eine Partei der anderen Partei in Verbindung mit der Verleihung des Status eines Organisationsmitglieds macht, sind Teil dieser Partnerbeziehung. Sofern nicht anders vereinbart ist, benötigt jede Partei die Zustimmung der anderen Partei, um diese Vereinbarungen zu ändern. Allerdings kann jede Partei die Partnerbeziehung auch ohne die Zustimmung der anderen Partei beenden. Dadurch werden die Rechte und Pflichten der betreffenden Person als Organisationsmitglied sofort beendet.

### 6.2 Arbeitsabsprachen

Die Organisationsmitglieder können ***Arbeitsabsprachen*** untereinander treffen. Dies sind Vereinbarungen darüber, wie die Beteiligter miteinander in Beziehung treten oder ihre allgemeinen Funktionen als Organisationsmitglied erfüllen. Sie müssen sich darauf konzentrieren, Verhaltensweisen zu formen, die im Allgemeinen der Arbeit zugrunde liegen. Sie dürfen weder Erwartungen an die Arbeit, die in einer Rolle zu erledigen ist, noch an die Prioritätensetzung eines Beteiligters in den verschiedenen Rollen, formulieren. Darüber hinaus dürfen sie nur konkrete Handlungen vorschreiben oder untersagen. Sie dürfen keine Versprechen enthalten, bestimmte Ergebnisse zu erreichen oder abstrakte Eigenschaften zu verkörpern.

Als Organisationsmitglied kannst du eine Arbeitsabsprache eines anderen Organisationsmitglieds für deine persönlichen Bedürfnisse oder für eine Rolle, die du einnimmst, angragen. Dieses Organisationsmitglied kann die gewünschte Arbeitsabsprache aufgrund seiner eigenen persönlichen Bedürfnisse akzeptieren oder ablehnen. Jede Partei kann die Vereinbarung später ohne die Zustimmung der anderen Partei kündigen, sofern nicht anders vereinbart.

Als Organisationsmitglied hast du die Pflicht, dein Verhalten entsprechend den getroffenen Arbeitsabsprachen zu gestalten. Jede Person, die ein Meeting oder einen Prozess moderiert, kann während dieses Meetings oder Prozesses auch eine Arbeitsabsprache durchsetzen, solange sie nicht im Widerspruch zu den Bestimmungen dieser Verfassung stehen.

---

**Anhang A**

## Kreisleitung

**Zielsetzung:** Die Kreisleitung verfolgt die Zielsetzung des gesamten Kreises.

**Befugnisse und Hoheit:**

- Rollenzuweisungen für alle Rollen des Kreises.

**Verantwortlichkeiten:**

Der Kreisleitung trägt alle Verantwortlichkeiten des Kreises, soweit sie nicht durch die Rollen oder Abläufe des Kreises abgedeckt sind.

## Kreisvertretung

**Zielsetzung:** Die Kreisvertretung kanalisiert und löst Spannungen im Kreis, die für den Prozessfluss in einem Oberkreis relevant sind.

**Verantwortlichkeiten:**

- Strebt danach, von Rollenträgern im Kreis mitgeteilte Spannungen zu verstehen.
- Arbeitet die Spannungen heraus, die für die Verarbeitung in einem Oberkreis relevant sind.
- Bringt Spannungen in den Oberkreis ein, um Hemmnisse des Kreises zu lösen.

## Moderation

**Zielsetzung:** Die Moderation gleicht die Arbeitsstruktur des Kreises und die operativen Vorgehensweisen mit den Regeln der Verfassung ab.

**Verantwortlichkeiten:**

- Moderiert den Strukturierungs-Prozess und die operativen Meetings des Kreises und seiner Rollen.
- Coacht andere Kreismitglieder zu den Regeln und Abläufen der Verfassung, auf Anfrage oder falls für effektive Meetings erforderlich.
- Prüft Meetings und Aufzeichnungen der Subkreise auf Anfrage und stellt falls nötig Prozessversagen fest.

## Schriftführung

**Zielsetzung:** Verwaltet die Arbeitsstruktur-Aufzeichnungen des Kreises und stabilisiert das Führen dieser Aufzeichnungen.

**Befugnisse und Hoheit:**

- Alle Arbeitsstruktur-Aufzeichnungen des Kreises.

**Verantwortlichkeiten:**

- Plant Strukturierungs- und operative Meetings des Kreises.
- Erfasst und publiziert Ergebnisse von Arbeitsstrukturierungs- und operativen Meetings.
- Stößt nach Ablauf der jeweiligen Amtszeit einer Rolle Neuwahlen an.
- Legt auf Anfrage die Verfassung aus, sowie alles was ihren Regeln untersteht.

---

Diese Satzung ist in neutraler Sprache geschrieben. Formulierungen wie etwa *das Teilnehmer* schließen alle Menschen ein.

Dieses Werk ist unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Namensnennung- ShareAlike 4.0 International License</a> lizenziert.

---
